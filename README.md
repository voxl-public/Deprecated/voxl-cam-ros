# VOXL Cam ROS

ROS Node for using cameras on the VOXL platform. See the [ModalAI Technical Documentation](https://docs.modalai.com/voxl-cam-ros/) for instructions on how to use.

To build, clone into a catkin workspace and build onboard VOXL or in the VOXL-Emulator docker image. For more details, see the [VOXL Nodes](https://gitlab.com/voxl-public/voxl-nodes/) package.